# -*- coding: utf-8 -*-
import vk_api
import datetime
from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType
from vk_api.utils import get_random_id

user_list = [455356105, 194437246]


def main():
    vk_session = vk_api.VkApi(token='token_group')
    vk = vk_session.get_api()

    longpoll = VkBotLongPoll(vk_session, '172647717')

    def add_user(id):
        user_list.append(id)

    def delete_user(id):
        user_list.remove(id)

    def check(array, f):
        for item in array:
            if item == f:
                return True
            else:
                pass
        return False

    for event in longpoll.listen():
        if event.type == VkBotEventType.MESSAGE_NEW:
            try:

                if event.obj.action['type'] == 'chat_invite_user':
                    user_id = event.obj.action['member_id']
                    # Способ Ивана
                    if not (check(user_list, user_id) == True):
                        vk.messages.removeChatUser(
                            chat_id=(event.obj.peer_id - 2000000000),
                            user_id=event.obj.from_id,
                            member_id=event.obj.action['member_id'],
                        )
            except TypeError:
                pass
            try:
                if event.obj.text.split('/id ')[0] == '':
                    user = int(event.obj.text.split('/id ')[1])
                    add_user(user)
                    print(f'добавил\nтеперь база:{user_list}')
                elif event.obj.text.split('/did ')[0] == '':
                    user = int(event.obj.text.split('/did ')[1])
                    delete_user(user)
                    print(f'удалил\nтеперь база:{user_list}')
            except (ValueError, IndexError):
                pass


if __name__ == '__main__':
    main()
